# Zsh config

## Install Zsh

 - sudo apt install zsh
 - chsh -s /bin/zsh

## Install Oh-my-zsh

```
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
```

The detail of Oh-my-zsh plugin, you can refer to [github](https://github.com/ohmyzsh/ohmyzsh)

## Install .zshrc

```
cp .zshrc ~/
```
