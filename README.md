# dotfiles

## Install

1. clone dotfiles to ~/
2. install stow (brew install stow)
3. cd dotfiles
  - stow zsh
  - stow nvim
  - stow tmux
  - stow misc
  - stow alacritty

## Uninstall

1. cd dotfiles
  - stow -D zsh
  - stow -D nvim
  - stow -D tmux
  - stow -D misc
  - stow -D alacritty

## NeoVim

 - [Releases · neovim/neovim](https://github.com/neovim/neovim/releases)
 - Install dependency
    - golang
    - unzip
    - npm
    - python3
    - ripgrep
    - universal-ctags
    - gotags
    - lazygit

Command
  - Lazy
  - Mason
  - TSUpdate
  - checkhealth

## Vim

Reference
  - https://blog.chh.tw/posts/vim-vundle/
  - http://terrychen.logdown.com/posts/2014/10/05/vundle-installation

Command
  - PluginInstall
  - PluginClean
  - PluginList

## Tmux

KeyBind
  - F11 previous window
  - F12 next window

## Screen

KeyBind
  - F11 previous screen
  - F12 next screen

## Tcsh

## Misc

### editrc

Reference
 - https://dev.mysql.com/doc/refman/5.6/en/mysql-tips.html
